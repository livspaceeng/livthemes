// import axios from 'axios';
import apiClient from "./apiClient";
export default {

    updateData(data){
          // console.log(data)
               const url = `https://campaign-service.herokuapp.com/api/v1/${data.type}s/${data.id}`;
               return apiClient.put(url,data.payload);
     //     await  axios.put(`https://campaign-service.herokuapp.com/api/v1/${data.type}s/${data.id}`,data.payload).then(() => {
     //           // this.fecthData(data.type);
     //      })
         
     },

     createData(data){
          const url = `https://campaign-service.herokuapp.com/api/v1/${data.type}s`;
          return apiClient.post(url,data.payload);
     },

    fetchData(type){
          const url = `https://campaign-service.herokuapp.com/api/v1/${type}s`;
          return apiClient.get(url);
          //    axios.get(`https://campaign-service.herokuapp.com/api/v1/${type}s`).then(response => {
     //           //  this.$bvToast.toast(`${type}created Succesfully`, {
     //           //      variant: 'success',
     //           //      solid: true,
     //           //      noCloseButton: true
     //           // })
     //           const data = response.data.data;
     //           return data;
     //           // this.$store.commit(`projects/SET_${type.toUpperCase()}_DATA`, data)
     //      });
         
     }
}