import Vue from 'vue'
import VueRouter from 'vue-router'

// import HelloWorld from './../components/HelloWorld.vue';
// import Source from './../components/HelloWorld.vue';
// import Theme from './../components/HelloWorld.vue';
import CreateCampaign from './../components/createCampaign.vue';
import Source from './../components/Source.vue';
import Theme from './../components/Theme.vue';
import Campaign from './../components/Campaign.vue';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Campaign
    },
    {
        path: '/theme',
        name: 'Theme',
        component: Theme
    },
    {
        path: '/source',
        name: 'Source',
        component: Source
    },
    {
        path: '/create-campaign',
        name: 'Create Campaign',
        component: CreateCampaign
    },
    {
        path: '/campaign',
        name: 'Campaign',
        component: Campaign,
        
    },
    //   {
    //     path: '/project',
    //     name: 'Project',
    //     component: Project,
    //     children: [
    //       {
    //         path: 'timeline',
    //         name: 'Timeline',
    //         component: Timeline
    //       },
    //       {
    //         path: 'updates',
    //         name: 'Updates',
    //         component: Updates
    //       },
    //       {
    //         path: 'team-member',
    //         name: 'Team Members',
    //         component: TeamMembers
    //       },
    //       {
    //         path: 'boqs',
    //         name: 'Boqs',
    //         component: Boqs
    //       },
    //       {
    //         path: 'payments',
    //         name: 'Payments',
    //         component: Payments
    //       },
    //       {
    //         path: 'orders',
    //         name: 'Orders',
    //         component: Orders
    //       },
    //       {
    //         path: 'documents',
    //         name: 'Documents',
    //         component: Documents
    //       },
    //       {
    //         path: 'wishlist',
    //         name: 'Wishlist',
    //         component: Wishlist
    //       },
    //       {
    //         path: 'grievances',
    //         name: 'Grievances',
    //         component: Grievances
    //       },
    //       {
    //         path: 'project-info',
    //         name: 'Project Info',
    //         component: ProjectInfo
    //       }
    //     ]
    //   },
    //   {
    //     path: '/discover',
    //     name: 'Catalogue',
    //     component: Catalogue
    //   },

    //   {
    //     path: '/referral',
    //     name: 'Referrals',
    //     component: Referrals
    //   },
    //   {
    //     path: '/notification',
    //     name: 'Notification',
    //     component: Updates
    //   },
    //   {
    //     path: '/callback',
    //     name: 'oidcCallback',
    //     component: AuthCallback,
    //     meta: {
    //       isVuexOidcCallback: true,
    //       isPublic: true
    //     }
    //   },
    //   {
    //     path: '/profile',
    //     name: 'Profile',
    //     component: Profile
    //   }
]

const router = new VueRouter({
    mode: 'history',
    base: '',
    routes
})

// router.beforeEach(vuexOidcCreateRouterMiddleware(store))
export default router