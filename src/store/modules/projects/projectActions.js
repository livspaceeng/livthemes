// import projectServices from './../../../services/project.services';
// import updateServices from './../../../services/update.service';
import service from './../../../service';

export default {
    async getData({ commit }, type) {
        const details = await service.fetchData(type);
          commit(`SET_${type.toUpperCase()}_DATA`, details.data.data)
        return details;
    },

}