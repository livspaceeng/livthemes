export default {
    SET_CAMPAIGN_DATA(state, data) {
        state.campaigndata = data;
    },
    SET_THEME_DATA(state, data) {
        state.themedata =  data ;
    },
    SET_SOURCE_DATA(state, data) {
        state.sourcedata = data;
    },
   
}