import state from './projectState'
import mutations from './projectMutations'
import getters from './projectGetters'
import actions from './projectActions'

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
}
