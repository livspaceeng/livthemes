import axios from 'axios'
import {EventBus} from './main'
const apiClient = axios.create({});

apiClient.CancelToken = axios.CancelToken
apiClient.isCancel = axios.isCancel

/*
 * The interceptor here ensures that we check for the token in local storage every time an ajax request is made
 */
apiClient.interceptors.request.use(
     (config) => {
          // console.log('insdfsefs',config)
          EventBus.$emit('show_spin');
          return config;
      },
      error =>
      {
          EventBus.$emit('hide_spin');
          return Promise.reject(error)
})

// apiClient.interceptors.response.use(response => {
//      NProgress.done()
//      return response;
// })
apiClient.interceptors.response.use( (response)=> {
     // console.log(response,'response',EventBus);
     EventBus.$emit('hide_spin');
     return response;
}, (error) =>{
     EventBus.$emit('hide_spin');
     return Promise.reject(error);
});
export default apiClient
