import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store';
// import * from 'https://cdn.jsdelivr.net/gh/jayrajkachariya/livspace-campaign-provider@30d95619bd14ea35182cba26b509f898dce2c05f/index.js'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import { MdButton, MdContent, MdTabs, MdList,MdDrawer } from 'vue-material/dist/components'
// import 'vue-material/dist/vue-material.min.css'
// import 'vue-material/dist/theme/default.css'
// import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'vue-loading-overlay/dist/vue-loading.css';
import './app.css';
// import './animation.css';
// import './animation'
import axios from 'axios'
// Vue.use(VueMaterial);
export const EventBus = new Vue();
Vue.config.productionTip = false
// Vue.use(MdButton)
// Vue.use(MdContent)
// Vue.use(MdList)
// Vue.use(MdDrawer)
// Vue.use(MdTabs)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(axios);
// Vue.loadScript('https://cdn.jsdelivr.net/gh/jayrajkachariya/livspace-campaign-provider@30d95619bd14ea35182cba26b509f898dce2c05f/index.js');


new Vue({
    store,
    router,
  render: h => h(App),
}).$mount('#app')

